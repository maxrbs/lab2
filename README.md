# Tunnel Race

## Description
### RU
Прототип игры «Гонка в туннеле»
Игрок движется по бесконечному туннелю, по мере прохождения пути - накапливаются очки и увеличивается скорость.
На пути появляются различные препятствия, при попадании на которые игрок проигрывает и забег начинается сначала.
### EN
Prototype of the game "Tunnel Race"
The player moves through an endless tunnel, as the path progresses - score and speed increases.
Various obstacles appear on the way, when hit on which the player loses and the race starts again.

## Screenshots
![Screen1](/Sharing/Screenshots/1.png)
![Screen2](/Sharing/Screenshots/2.png)
![Screen3](/Sharing/Screenshots/3.png)
![Screen4](/Sharing/Screenshots/4.png)

## Links
Build on itch: https://maxrbs.itch.io/tunnel-race