﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{
    public Transform player;                          //ссылка на игрока
    public Text scoreText;                            //ссылка на текст UI с очками
    public Text fpsText;                              //ссылка на текст UI с fps
    public Text finalScoreText;                       //ссылка на текст с финальным значением score
    public GameController gameController;             //ссылка на gameController

    public int HP = 1;                                //жизни игрока
    public int Score = 0;                             //количество очков игрока
    public int scoreIncreaseDelta = 10;               //на сколько увеличивать очки игрока
    public float scoreIncreaseDistance = 100f;        //через сколько метров увеличивать очки игрока
    public float forwardVelocityIncreaseDelta = 0.1f; //на сколько увеличивать скорость вперед игрока
    public float rotateVelocityIncreaseDelta = 0.05f; //на сколько увеличивать скорость поворота игрока
    public float velocityIncreaseTime = 5f;           //через сколько секунд увеличивать скорость игрока

    private PlayerMoving playerMoving;
    private float nextScorePoint;                     //координата след. точки добавления очков

    private float StartSpeed;
    private int StartHP;
    
    // Start is called before the first frame update
    void Start()
    {
        playerMoving = player.gameObject.GetComponent<PlayerMoving>();

        StartSpeed = playerMoving.forwardSpeed;
        StartHP = HP;

        nextScorePoint = scoreIncreaseDistance;
        scoreText.text = "Score: " + "0";
        StartCoroutine(VelocityIncreaser());
        StartCoroutine(FPSCounter());
    }


    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + Score.ToString();
        finalScoreText.text = "Total score: " + Score.ToString();

        if (nextScorePoint - player.position.z > scoreIncreaseDistance * 2)
        {
            nextScorePoint = scoreIncreaseDistance;
        }

        if (player.position.z >= nextScorePoint)
        {
            if (HP > 0)
                Score += scoreIncreaseDelta;
            
            nextScorePoint += scoreIncreaseDistance;
        }

    }

    IEnumerator FPSCounter()
    {
        while (true)
        {
            fpsText.text = "FPS: " + ((int)(1f / Time.deltaTime)).ToString();
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator VelocityIncreaser()
    {
        while (HP > 0)
        {
            playerMoving.rotateSpeed += rotateVelocityIncreaseDelta;
            playerMoving.forwardSpeed += forwardVelocityIncreaseDelta;
            yield return new WaitForSeconds(velocityIncreaseTime);
        }
    }
}
