﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastingForward : MonoBehaviour
{
    public PlayerData playerData;           //ссылка на данные об игроке
    public float raycastLength = 1f;        //длина лучей рейкаста
    private int layerMask = 1 << 8;         //маска для рейкаста
    
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, raycastLength, layerMask))
        {
            Destroy(hit.collider.gameObject);
            print("Got Hit!");
            playerData.HP -= 1;

            if (playerData.HP == 0)
                playerData.gameController.PlayerLose();
            
        }
    }
}
