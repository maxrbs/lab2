﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstackleCreator : MonoBehaviour
{
    public Transform player;                                           //ссылка на игрока

    public Transform[] pointOfSpawn = new Transform[8];                //точки для спавна 
    public GameObject[] obstacklePrefabs;                              //префабы препятствий  

    public float distanceToDeleteObject = 15f;                         //расстояние позади игрока, на котором уничтожится барьер
    public float spawnDistanceFromPlayer = 150f;                       //расстояние от игрока, на котором спавнится препятствие
    public float minSpawnTime = 0.4f;                                  //минимальное время спавна 
    public float maxSpawnTime = 2f;                                    //максимальное время спавна

    public Transform motherOfObstacles;                                //контейнер для заспавненных объектов
    public float distanceToNextSpawn = 30f;

    private float nextSpawnTime;                                       //следующее время спавна объектов
    private float nextSpawnPointZ;
    private GameObject lastSpawnedObstackle;


    void Start()
    {
        nextSpawnTime = maxSpawnTime;
        nextSpawnPointZ = distanceToNextSpawn;
        lastSpawnedObstackle = player.gameObject;
    }

    void Update()
    {
        if (Time.timeScale != 0f)
        {
            if (player.position.z > nextSpawnPointZ)
            {
                SpawnObstacle();
                nextSpawnPointZ += distanceToNextSpawn; //плюс велосити
            }        
        }
    }


    void SpawnObstacle()
    {
        Vector3 coord;
        Vector3 rotation;

        GameObject pref = obstacklePrefabs[Random.Range(0, obstacklePrefabs.Length)];

        if (lastSpawnedObstackle == null)
            lastSpawnedObstackle = player.gameObject;

        if (pref.tag == "atCentreObstacle")
        {
            coord = new Vector3(0f, 0f, lastSpawnedObstackle.transform.position.z + distanceToNextSpawn);
            
            rotation = Vector3.forward * Random.Range(0, 3) * 45f;
        }
        else
        {
            int numOfPoint = Random.Range(0, 7);
            coord = new Vector3(pointOfSpawn[numOfPoint].position.x, pointOfSpawn[numOfPoint].position.y, lastSpawnedObstackle.transform.position.z + distanceToNextSpawn);

            rotation = pointOfSpawn[numOfPoint].rotation.eulerAngles;
        }

        if (lastSpawnedObstackle == player.gameObject)
            coord.z += spawnDistanceFromPlayer;

        lastSpawnedObstackle = Instantiate(pref, coord, Quaternion.Euler(rotation));
        lastSpawnedObstackle.transform.SetParent(motherOfObstacles);
        lastSpawnedObstackle.transform.SetParent(motherOfObstacles);

        ObstackleScript obstData = lastSpawnedObstackle.GetComponent<ObstackleScript>();
        obstData.minDistanceToDelete = distanceToDeleteObject;
        obstData.player = player;

    }


    public void MoveObstacles()
    {
        print("lap");

        //перемещение объектов 
        for (int i = 0; i < motherOfObstacles.childCount; i++)
        {
            
            float zz = player.position.z - motherOfObstacles.GetChild(i).position.z;        //расстояние между персонажем и данным объектом
            
            motherOfObstacles.GetChild(i).transform.position = new Vector3(motherOfObstacles.GetChild(i).transform.position.x,
                                                                           motherOfObstacles.GetChild(i).transform.position.y,
                                                                           0 - zz);
            
        }

        nextSpawnPointZ = 0f - (player.position.z - nextSpawnPointZ);
    }
}
