﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstackleScript : MonoBehaviour
{
    public Transform player;                         //ссылка на игрока
    public float minDistanceToDelete = 5f;           //расстояние от игрока, на котором 

    // Update is called once per frame
    void Update()
    {
        if (player.position.z - transform.position.z > minDistanceToDelete)
            Destroy(gameObject);
    }

}
