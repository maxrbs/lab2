﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject gameUI;           //ссылка на UI с игровыми объектами
    public GameObject pauseUI;          //ссылка на UI с паузой
    public GameObject loseUI;           //ссылка на UI с проигрышем

    // Start is called before the first frame update
    void Start()
    {
        gameUI.SetActive(true);
        pauseUI.SetActive(false);
        loseUI.SetActive(false);
        Time.timeScale = 1f;

        //отключение курсора
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !loseUI.activeSelf)
            Pausing();
    }

    //пауза игры
    public void Pausing()
    {
        //продолжение игры
        if (Time.deltaTime == 0f)
        {
            //включение курсора
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            gameUI.SetActive(true);
            pauseUI.SetActive(false);
            Time.timeScale = 1f;
        }
        //пауза игры
        else
        {
            //выключение курсора
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            gameUI.SetActive(false);
            pauseUI.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    //перезапуск игры
    public void Restarting()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    //обработчик проигрыша игрока
    public void PlayerLose()
    {
        //включение курсора
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        gameUI.SetActive(false);
        pauseUI.SetActive(false);
        loseUI.SetActive(true);
        Time.timeScale = 0f;
    }

    //выход из игры
    public void ExitGame()
    {
        Application.Quit();
    }
}

