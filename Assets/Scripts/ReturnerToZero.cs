﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnerToZero : MonoBehaviour
{
    public float distanceToReload = 500f;          //дистанция, при достижении которой все объекты сместятся в нуль
    private Transform player;                      //ссылка на игрока
    private ObstackleCreator obstackleCreator;     //ссылка на создателя препятсвий
    private WorldCreator worldCreator;             //ссылка на создателя окружения


    // Start is called before the first frame update
    void Start()
    {
        obstackleCreator = GetComponent<ObstackleCreator>();
        worldCreator = GetComponent<WorldCreator>();
        player = worldCreator.player;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player.position.z > distanceToReload)
        {
            obstackleCreator.MoveObstacles();
            worldCreator.MoveWorld();
            player.position = new Vector3(player.transform.position.x, player.transform.position.y, 0f);
        }
    }
}
