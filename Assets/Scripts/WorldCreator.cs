﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCreator : MonoBehaviour
{
    public Transform player;                                           //ссылка на игрока
    public GameObject tilePrefab;                                      //префаб части тоннеля
    public float tileLength;                                           //длина одной части тоннеля
    private float zSpawn = 0f;                                         //следующая позиция для спавна
    public int numberOfTiles = 5;                                      //количество тайлов на карте одновременно
    private List<GameObject> activeTiles = new List<GameObject>();     //ссылки на тайлы на карте
    public Transform motherOfTiles;                                    //контейнер для тайлов


    void Start()
    {
        SpawnTile(-1);      //создание начальных n тайлов

    }

    void Update()
    {
        if (player.position.z - (tileLength + 5) > zSpawn - (numberOfTiles * tileLength))
        {
            SpawnTile(0);
            DeleteTile();
        }
        
    }

    public void SpawnTile(int tileIndex)
    {
        if (tileIndex == -1)        //создание карты в начале
        {
            for (int i = 0; i < numberOfTiles; i++)
            {
                SpawnTile(0);
            }
        }
        else                        //создание карты после начала
        {
            GameObject go = Instantiate(tilePrefab, transform.forward * zSpawn, transform.rotation);
            go.transform.Rotate(90f, 0f, 0f);
            go.transform.SetParent(motherOfTiles);
            activeTiles.Add(go);
            zSpawn += tileLength;
        }
    }


    private void DeleteTile(int num = 1)
    {
        if (num == -1)
        {
            for (int i = 0; i < numberOfTiles; i++)
            {

                Destroy(activeTiles[0]);     //удаляем со сцены
                activeTiles.RemoveAt(0);     //удаляем со списка
            }
            zSpawn = 0;
        }
        else
        {
            Destroy(activeTiles[0]);     //удаляем со сцены
            activeTiles.RemoveAt(0);     //удаляем со списка
        }
    }


    public void MoveWorld()
    {
        DeleteTile(-1);
        SpawnTile(-1);
    }
}
