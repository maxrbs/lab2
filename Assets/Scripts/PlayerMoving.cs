﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoving : MonoBehaviour
{
    public float forwardSpeed = 3f;             //скорость перемещения вперед
    public float rotateSpeed = 3f;              //на сколько каждый кадр поворачивать игрока
    public float changeLineAngle = 45f;         //угол поворота для смены полосы
    public float currentLine = 0f;              //текущая полоса
    private bool animLock = false;              //блок управления при проигрывании анимации


    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 0f)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                ChangeLine(-1);
            if (Input.GetKeyDown(KeyCode.RightArrow))
                ChangeLine(1);

            transform.Translate(Vector3.forward * forwardSpeed * Time.deltaTime);
        }
    }

    
    void ChangeLine(int dir)
    {
        if (!animLock)
        {
            currentLine += dir;
            if (currentLine == -1) currentLine = 7;
            if (currentLine == 8) currentLine = 0;

            StartCoroutine(Rotation(dir));
        }
    }

    IEnumerator Rotation(int dir)
    {
        //dir == -1 - поворот по часовой (движение влево)
        //dir ==  1 - поворот против часовой (движение вправо)
        animLock = true;

        float total_angle = 0f;
        while (total_angle < changeLineAngle)
        {
            transform.Rotate(Vector3.forward, rotateSpeed * dir * Time.deltaTime);
            total_angle += rotateSpeed * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        transform.rotation = Quaternion.Euler(0f, 0f, currentLine * 45f);
        animLock = false;
    }
}
